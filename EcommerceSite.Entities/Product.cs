﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcommerceSite.Entities
{
   public class Product :BaseEntity
    {
        public string size { get; set; }
        public decimal price { get; set; }
        public Category Category { get; set; }

    }
}
